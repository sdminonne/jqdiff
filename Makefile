

jqdiff: main.go ./pkg/jqdiff/jqdiff.go
	go build .

test: ./pkg/jqdiff/jqdiff_test.go
	go test ./...

cover:
	go test -coverprofile=coverage.out ./...

view-cover:
	go tool cover -html=coverage.out

clean:
	rm -fr jqdiff
