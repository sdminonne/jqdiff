# jqdiff

[![coverage report](https://gitlab.com/sdminonne/jqdiff/badges/main/coverage.svg?style=flat-square)](https://gitlab.com/sdminonne/jqdiff/commits/main)

`jqdiff` is a diff tool for json files. It returns differences following `jq` element selection syntax
