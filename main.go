package main

import (
	"flag"
	"fmt"
	"io"
	"os"

	"github.com/sdminonne/jqdiff/pkg/jqdiff"
)

func main() {

	lhsFileName := flag.String("lhs-file", "", "")
	rhsFileName := flag.String("rhs-file", "", "")
	flag.Parse()

	lhsFile, err := os.Open(*lhsFileName)
	if err != nil {
		fmt.Printf("unable to open %s", *lhsFileName)
		return
	}
	defer lhsFile.Close()
	lhsContent, err := io.ReadAll(lhsFile)
	if err != nil {
		fmt.Printf("unable to read %s", *lhsFileName)
		return
	}

	rhsFile, err := os.Open(*rhsFileName)
	if err != nil {
		fmt.Printf("unable to read %s", *rhsFileName)
	}
	defer rhsFile.Close()
	rhsContent, err := io.ReadAll(rhsFile)
	if err != nil {
		fmt.Printf("unabel to read %s", *rhsFileName)
	}

	diffs, err := jqdiff.Compare(lhsContent, rhsContent)
	if err != nil {
		fmt.Printf("couldn't compare %s and %s: %v", *lhsFileName, *rhsFileName, err)
	}

	for i := range diffs {
		fmt.Printf("%v", diffs[i])
	}

}
