package jqdiff

import (
	"reflect"
	"testing"
)

func Test_Basic_Types_jqondiff_Compare(t *testing.T) {
	type args struct {
		lhs []byte
		rhs []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []Diff
		wantErr bool
	}{
		{
			name: "just for coverage: 1",
			args: args{
				lhs: []byte(`this is not json`),
				rhs: []byte(`"value1"`),
			},
			want:    []Diff{},
			wantErr: true,
		},
		{
			name: "just for coverage: 2",
			args: args{
				lhs: []byte(`"value1"`),
				rhs: []byte(`this is not json neither`),
			},
			want:    []Diff{},
			wantErr: true,
		},
		{
			name: "flat strings: No diff",
			args: args{
				lhs: []byte(`"value1"`),
				rhs: []byte(`"value1"`),
			},
			want:    []Diff{},
			wantErr: false,
		},
		{
			name: "flat strings: 1 diff",
			args: args{
				lhs: []byte(`"value1"`),
				rhs: []byte(`"value2"`),
			},
			want: []Diff{typedDiff[string, string]{
				Selector: ".",
				Rhs:      "value2",
				Lhs:      "value1",
				Kind:     DifferentValue}},
			wantErr: false,
		},
		{
			name: "flat strings: 1 diff",
			args: args{
				lhs: []byte(`"value1"`),
				rhs: []byte(`null`),
			},
			want: []Diff{typedDiff[string, interface{}]{
				Selector: ".",
				Rhs:      nil,
				Lhs:      "value1",
				Kind:     DifferentType}},
			wantErr: false,
		},
		{
			name: "flat bools: No diff",
			args: args{
				lhs: []byte(`true`),
				rhs: []byte(`true `),
			},
			want:    []Diff{},
			wantErr: false,
		},
		{
			name: "flat bools: 1 diff",
			args: args{
				lhs: []byte(`true`),
				rhs: []byte(`false `),
			},
			want: []Diff{typedDiff[bool, bool]{
				Selector: ".",
				Lhs:      true,
				Rhs:      false,
				Kind:     DifferentValue,
			}},
			wantErr: false,
		},
		{
			name: "flat bools: 1 diff wrong type",
			args: args{
				lhs: []byte(`true`),
				rhs: []byte(`"true"`),
			},
			want: []Diff{typedDiff[bool, any]{
				Selector: ".",
				Lhs:      true,
				Rhs:      "true",
				Kind:     DifferentType,
			}},
			wantErr: false,
		},
		{
			name: "flat float64s: No diff",
			args: args{
				lhs: []byte(`1`),
				rhs: []byte(`1`),
			},
			want:    []Diff{},
			wantErr: false,
		},
		{
			name: "flat float64s: 1 diff",
			args: args{
				lhs: []byte(`1`),
				rhs: []byte(`2`),
			},
			want: []Diff{typedDiff[float64, float64]{
				Selector: ".",
				Lhs:      1,
				Rhs:      2,
				Kind:     "DifferentValue"}},
			wantErr: false,
		},
		{
			name: "flat float64s: 1 diff",
			args: args{
				lhs: []byte(`1`),
				rhs: []byte(`"two"`),
			},
			want: []Diff{typedDiff[float64, any]{
				Selector: ".",
				Lhs:      1,
				Rhs:      "two",
				Kind:     "DifferentType"}},
			wantErr: false,
		},
		{
			name: "flat nulls: No diff",
			args: args{
				lhs: []byte(`null`),
				rhs: []byte(`null`),
			},
			want:    []Diff{},
			wantErr: false,
		},
		{
			name: "flat nulls: 1 diff",
			args: args{
				lhs: []byte(`null`),
				rhs: []byte(`true`),
			},
			want: []Diff{typedDiff[any, any]{
				Selector: ".",
				Lhs:      nil,
				Rhs:      true,
				Kind:     DifferentType,
			}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Compare(tt.args.lhs, tt.args.rhs)
			if (err != nil) != tt.wantErr {
				t.Errorf("Compare() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Compare() got = %#v\n want = %#v", got, tt.want)
			}
		})
	}
}

func Test_Composed_Types_jqondiff_Compare(t *testing.T) {
	type args struct {
		lhs []byte
		rhs []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []Diff
		wantErr bool
	}{
		{
			name: "string in object: 1 diff",
			args: args{
				lhs: []byte(`{ "key": "value1" }`),
				rhs: []byte(`{ "key": "value2" }`),
			},
			want: []Diff{typedDiff[string, string]{
				Selector: ".key",
				Lhs:      "value1",
				Rhs:      "value2",
				Kind:     DifferentValue}},
			wantErr: false,
		},
		{
			name: "string in object No diff",
			args: args{
				lhs: []byte(`{ "key1": "true" }`),
				rhs: []byte(`{ "key1": "true" }`),
			},
			want:    []Diff{},
			wantErr: false,
		},
		{
			name: "object 1 diff - wrong type",
			args: args{
				lhs: []byte(`{ "key1": "true" }`),
				rhs: []byte(`"just a string"`),
			},
			want: []Diff{typedDiff[map[string]interface{}, interface{}]{
				Selector: ".",
				Lhs:      map[string]interface{}{"key1": "true"},
				Rhs:      "just a string",
				Kind:     "DifferentType"}},
			wantErr: false,
		},
		{
			name: "bool in object No diff",
			args: args{
				lhs: []byte(`{ "key1": true }`),
				rhs: []byte(`{ "key1": true }`),
			},
			want:    []Diff{},
			wantErr: false,
		},

		{
			name: "float64 in object No diff",
			args: args{
				lhs: []byte(`{ "key1": 1 }`),
				rhs: []byte(`{ "key1": 1 }`),
			},
			want:    []Diff{},
			wantErr: false,
		},
		{
			name: "null in object No diff",
			args: args{
				lhs: []byte(`{ "key1": null }`),
				rhs: []byte(`{ "key1": null }`),
			},
			want:    []Diff{},
			wantErr: false,
		},
		{
			name: "null in object 1 diff",
			args: args{
				lhs: []byte(`{ "key1": null }`),
				rhs: []byte(`{ "key1": true }`),
			},
			want: []Diff{typedDiff[any, any]{
				Selector: ".key1",
				Lhs:      nil,
				Rhs:      true,
				Kind:     DifferentType,
			}},
			wantErr: false,
		},
		{
			name: ".key.subkey No diff",
			args: args{
				lhs: []byte(`{ "key": { "subkey" : true } }`),
				rhs: []byte(`{ "key": { "subkey" : true } }`),
			},
			want:    []Diff{},
			wantErr: false,
		},
		{
			name: ".key 1 diff: 1 missing object",
			args: args{
				lhs: []byte(`{ "key1": "one", "key2": "two" }`),
				rhs: []byte(`{ "key1": "one" }`),
			},
			want: []Diff{typedDiff[interface{}, interface{}]{
				Selector: ".key2",
				Lhs:      "two",
				Kind:     "MissingObject",
			}},
			wantErr: false,
		},
		{
			name: ".key.subkey 1 diff value",
			args: args{
				lhs: []byte(`{ "key": { "subkey" : true } }`),
				rhs: []byte(`{ "key": { "subkey" : false } }`),
			},
			want: []Diff{typedDiff[bool, bool]{
				Selector: ".key.subkey",
				Rhs:      false,
				Lhs:      true,
				Kind:     DifferentValue,
			}},
			wantErr: false,
		},
		{
			name: ".key.subkey.sub-subkey 1 diff value",
			args: args{
				lhs: []byte(`{ "key": { "subkey" : { "sub-subkey": true } } }`),
				rhs: []byte(`{ "key": { "subkey" : { "sub-subkey": false } } }`),
			},
			want: []Diff{typedDiff[bool, bool]{
				Selector: ".key.subkey.sub-subkey",
				Rhs:      false,
				Lhs:      true,
				Kind:     DifferentValue,
			}},
			wantErr: false,
		},
		{
			name: ".key2 unexpected object",
			args: args{
				lhs: []byte(`{ "key1": true  }`),
				rhs: []byte(`{ "key1": true, "key2": false  }`),
			},
			want: []Diff{typedDiff[any, any]{
				Selector: ".key2",
				Rhs:      false,
				Lhs:      interface{}(nil),
				Kind:     UnexpectedObject,
			}},
			wantErr: false,
		},
		{
			name: "array: no diff",
			args: args{
				lhs: []byte(`["one", "two", "three"]`),
				rhs: []byte(`["one", "two", "three"]`),
			},
			want:    []Diff{},
			wantErr: false,
		},
		{
			name: "array: 1 diff",
			args: args{
				lhs: []byte(`["one", "two", "three"]`),
				rhs: []byte(`["one", "due", "three"]`),
			},
			want: []Diff{
				typedDiff[string, string]{
					Selector: ".[1]",
					Rhs:      "due",
					Lhs:      "two",
					Kind:     DifferentValue,
				}},
			wantErr: false,
		},
		{
			name: "array: 1 diff: 1 more lhs",
			args: args{
				lhs: []byte(`["one", "two", "three"]`),
				rhs: []byte(`["one", "two"]`),
			},
			want: []Diff{
				typedDiff[any, any]{
					Selector: ".[2]",
					Lhs:      "three",
					Rhs:      interface{}(nil),
					Kind:     MissingValue,
				}},
			wantErr: false,
		},
		{
			name: "array: 1 diff: 2 more lhs",
			args: args{
				lhs: []byte(`["one", "two", "three", "four"]`),
				rhs: []byte(`["one", "two"]`),
			},
			want: []Diff{
				typedDiff[any, any]{
					Selector: ".[2]",
					Lhs:      "three",
					Rhs:      interface{}(nil),
					Kind:     MissingValue,
				},
				typedDiff[any, any]{
					Selector: ".[3]",
					Lhs:      "four",
					Rhs:      interface{}(nil),
					Kind:     MissingValue,
				}},
			wantErr: false,
		},
		{
			name: "array: 1 diff - wrong type",
			args: args{
				lhs: []byte(`["one", "two", "three"]`),
				rhs: []byte(`"just a string"`),
			},
			want: []Diff{
				typedDiff[[]any, any]{
					Selector: ".",
					Lhs:      []interface{}{"one", "two", "three"},
					Rhs:      "just a string",
					Kind:     DifferentType,
				}},
			wantErr: false,
		},
		{
			name: "array: 2 diff",
			args: args{
				lhs: []byte(`["one", "two", "three", "four", "five", "six"]`),
				rhs: []byte(`["one", "due", "three", "four", "cinq", "six"]`),
			},
			want: []Diff{
				typedDiff[string, string]{
					Selector: ".[1]",
					Rhs:      "due",
					Lhs:      "two",
					Kind:     DifferentValue,
				},
				typedDiff[string, string]{
					Selector: ".[4]",
					Rhs:      "cinq",
					Lhs:      "five",
					Kind:     DifferentValue,
				}},
			wantErr: false,
		},
		{
			name: "array: 2 diff more rhs",
			args: args{
				lhs: []byte(`["one", "two", "three", "four"]`),
				rhs: []byte(`["one", "two", "three", "four", "five", "six"]`),
			},
			want: []Diff{typedDiff[any, any]{
				Selector: ".[4]",
				Rhs:      "five",
				Lhs:      interface{}(nil),
				Kind:     UnexpectedValue,
			},
				typedDiff[any, any]{
					Selector: ".[5]",
					Rhs:      "six",
					Lhs:      interface{}(nil),
					Kind:     UnexpectedValue,
				}},
			wantErr: false,
		},
		{
			name: "array of arrays: no diff",
			args: args{
				lhs: []byte(`[ ["one", "two"] , ["three", "four"], ["five", "six"] ]`),
				rhs: []byte(`[ ["one", "two"] , ["three", "four"], ["five", "six"] ]`),
			},
			want:    []Diff{},
			wantErr: false,
		},
		{
			name: "array of arrays: 1 diff",
			args: args{
				lhs: []byte(`[ ["one", "two"] , ["three", "four"], ["five", "six"] ]`),
				rhs: []byte(`[ ["one", "two"] , ["trois", "four"], ["five", "six"] ]`),
			},
			want: []Diff{
				typedDiff[string, string]{
					Selector: ".[1][0]",
					Rhs:      "trois",
					Lhs:      "three",
					Kind:     DifferentValue,
				}},
			wantErr: false,
		},
		{
			name: "array of arrays: 3 diff",
			args: args{
				lhs: []byte(`[ ["one", "two"] , ["three", "four"], ["five", "six"] ]`),
				rhs: []byte(`[ ["one", "two"] , ["trois", "four"], ["six", "cinq"] ]`),
			},
			want: []Diff{
				typedDiff[string, string]{
					Selector: ".[1][0]",
					Rhs:      "trois",
					Lhs:      "three",
					Kind:     DifferentValue,
				},
				typedDiff[string, string]{
					Selector: ".[2][0]",
					Rhs:      "six",
					Lhs:      "five",
					Kind:     DifferentValue,
				},
				typedDiff[string, string]{
					Selector: ".[2][1]",
					Rhs:      "cinq",
					Lhs:      "six",
					Kind:     DifferentValue,
				},
			},
			wantErr: false,
		},

		// '[true, "mixed array", 12, { "key": null }, ["not", "mixed", "array"] ]'
		// '{ "key": [true, null, "hello", false, "world" ] }'
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Compare(tt.args.lhs, tt.args.rhs)
			if (err != nil) != tt.wantErr {
				t.Errorf("Compare() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Compare()\n got  -> %#v\n want -> %#v", got, tt.want)
			}
		})
	}
}
