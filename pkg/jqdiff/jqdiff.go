package jqdiff

import (
	"encoding/json"
	"fmt"
	"reflect"
)

type DifferenceKind string

const (
	DifferentValue   DifferenceKind = "DifferentValue"
	DifferentType    DifferenceKind = "DifferentType"
	MissingValue     DifferenceKind = "MissingValue"
	UnexpectedValue  DifferenceKind = "UnexpectedValue"
	UnexpectedObject DifferenceKind = "UnexpectedObject"
	MissingObject    DifferenceKind = "MissingObject"
)

type Diff interface {
}

type typedDiff[R any, A any] struct {
	// selector contains the jq path to select the elment
	Selector string
	// Lhs is the left hand side difference
	Lhs R
	// Rhs is the right hand side difference
	Rhs A
	// Kind is the kind of difference
	Kind DifferenceKind
}

func Compare(lhs, rhs []byte) ([]Diff, error) {
	var lhsData any
	err := json.Unmarshal([]byte(lhs), &lhsData)
	if err != nil {
		return []Diff{}, fmt.Errorf("can't unmarshal lhs data: %v", err)
	}

	var rhsData any
	err = json.Unmarshal([]byte(rhs), &rhsData)
	if err != nil {
		return []Diff{}, fmt.Errorf("can't unmarshal rhs data: %v", err)
	}
	return compareAnyData(".", lhsData, rhsData), nil
}

func compareAnyData(element string, lhsData, rhsData any) []Diff {
	switch r := lhsData.(type) {
	case nil: // it handles json 'null'
		if reflect.TypeOf(rhsData) == nil {
			return []Diff{}
		}
		return []Diff{typedDiff[any, any]{
			Selector: element,
			Lhs:      lhsData,
			Rhs:      rhsData,
			Kind:     DifferentType}} // typeError since rhsData is not a bool
	case bool:
		if a, ok := rhsData.(bool); ok {
			return compareDataBool(element, r, a)
		}
		return []Diff{typedDiff[bool, any]{
			Selector: element,
			Lhs:      lhsData.(bool),
			Rhs:      rhsData,
			Kind:     DifferentType, // typeError since rhsData is not a bool
		}}
	case float64:
		if a, ok := rhsData.(float64); ok {
			return compareDataFloat64(element, r, a)
		}
		return []Diff{typedDiff[float64, any]{
			Selector: element,
			Lhs:      lhsData.(float64),
			Rhs:      rhsData,
			Kind:     DifferentType, // typeError since rhsData is not a float64
		}}
	case string:
		if a, ok := rhsData.(string); ok {
			return compareDataStrings(element, r, a)
		}
		return []Diff{
			typedDiff[string, any]{
				Selector: element,
				Lhs:      lhsData.(string),
				Rhs:      rhsData,
				Kind:     DifferentType, // typeError since rhsData is not a string
			}}
	case []interface{}:
		if a, ok := rhsData.([]interface{}); ok {
			return compareDataArrays(element, r, a)
		}
		return []Diff{
			typedDiff[[]interface{}, any]{
				Selector: element,
				Lhs:      lhsData.([]interface{}),
				Rhs:      rhsData,
				Kind:     DifferentType, // typeError since rhsData is not a []interface{}
			}}
	case map[string]interface{}:
		if a, ok := rhsData.(map[string]interface{}); ok {
			return compareDataMaps(element, r, a)
		}
		return []Diff{typedDiff[map[string]interface{}, any]{
			Selector: element,
			Lhs:      lhsData.(map[string]interface{}),
			Rhs:      rhsData,
			Kind:     DifferentType, // typeError since rhsData is not a map[string]interface{}
		}}
	default:
		panic(fmt.Sprintf("-> Hey dude I cannot handle type: %#v\n", reflect.TypeOf(lhsData)))
	}
}

func compareDataBool(element string, lhs, rhs bool) []Diff {
	if lhs != rhs {
		return []Diff{
			typedDiff[bool, bool]{
				Selector: element,
				Rhs:      rhs,
				Lhs:      lhs,
				Kind:     DifferentValue},
		}
	}
	return []Diff{}
}

func compareDataFloat64(element string, lhs, rhs float64) []Diff {
	if lhs != rhs {
		return []Diff{
			typedDiff[float64, float64]{
				Selector: element,
				Rhs:      rhs,
				Lhs:      lhs,
				Kind:     DifferentValue},
		}
	}
	return []Diff{}
}

func compareDataStrings(element string, lhs, rhs string) []Diff {
	if lhs != rhs {
		return []Diff{
			typedDiff[string, string]{
				Selector: element,
				Rhs:      rhs,
				Lhs:      lhs,
				Kind:     DifferentValue},
		}
	}
	return []Diff{}
}

func compareDataArrays(element string, lhs, rhs []interface{}) []Diff {
	diffs := []Diff{}
	lhsLength := len(lhs)
	rhsLength := len(rhs)
	for i := 0; i < lhsLength; i++ {
		currentElement := fmt.Sprintf("%s[%d]", element, i)
		switch {
		case i < rhsLength:
			diffs = append(diffs, compareAnyData(currentElement, lhs[i], rhs[i])...)
		case i >= rhsLength:
			diffs = append(diffs, typedDiff[any, any]{
				Selector: currentElement,
				Lhs:      lhs[i],
				Rhs:      nil,
				Kind:     MissingValue,
			})
		}
	}
	for i := lhsLength; i < rhsLength; i++ {
		currentElement := fmt.Sprintf("%s[%d]", element, i)
		diffs = append(diffs, typedDiff[any, any]{
			Selector: currentElement,
			Lhs:      nil,
			Rhs:      rhs[i],
			Kind:     UnexpectedValue,
		})
	}
	return diffs
}

func compareDataMaps(element string, lhs, rhs map[string]interface{}) []Diff {
	// need to run both side
	diffs := []Diff{}
	for lhsKey, lhsValue := range lhs {
		currentSelector := "." + lhsKey
		if element[len(element)-1:] != "." {
			currentSelector = element + "." + lhsKey
		}
		rhsValue, ok := rhs[lhsKey]
		if ok {
			diffs = append(diffs, compareAnyData(currentSelector, lhsValue, rhsValue)...)
			continue
		}
		diffs = append(diffs, typedDiff[any, any]{
			Selector: currentSelector,
			Lhs:      lhsValue,
			Rhs:      nil,
			Kind:     MissingObject, // missing key/value in rhs
		})
	}
	for rhsKey, rhsValue := range rhs {
		currentSelector := "." + rhsKey
		if element[len(element)-1:] != "." {
			currentSelector = element + "." + rhsKey
		}
		if _, ok := lhs[rhsKey]; !ok {
			diffs = append(diffs, typedDiff[any, any]{
				Selector: currentSelector,
				Lhs:      nil,
				Rhs:      rhsValue,
				Kind:     UnexpectedObject, // missing key/value in lhs
			})
		}
	}
	return diffs
}
